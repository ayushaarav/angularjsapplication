angular.module('myApp', []).controller('customersCtrl', function($scope) {
    $scope.names = [
        {serviceAccount:'9730901001',IncidentID:'8565346' , incidentType: 'Barred' , status:'Closed', comment:'Activated' , creation: 'yyyy-mm-dd'},
        {serviceAccount:'9730901000',IncidentID:'8565345' , incidentType: 'Barred' , status:'Pending', comment:'Under Monitor' , creation: 'yyyy-mm-dd'},
        {serviceAccount:'9730901000',IncidentID:'8565345' , incidentType: 'Barred' , status:'Open', comment:'Assigned to Engineer' , creation: 'yyyy-mm-dd'},
    ];
}); 